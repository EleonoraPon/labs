#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
    sf::CircleShape shape(100.f);
    sf::CircleShape shapeW(50.f);
    sf::CircleShape shapeY(150.f);

    shape.setFillColor(sf::Color::Green);
    shapeW.setFillColor(sf::Color::White);
    shapeY.setFillColor(sf::Color::Yellow);

    shape.setOrigin(100, 100);
    shapeW.setOrigin(50, 50);
    shapeY.setOrigin(150, 150);

    int shape_y = 100, shapeW_y = 50, shapeY_y = 150;
    shape.setPosition(100, shape_y);
    shapeW.setPosition(300, shapeW_y);
    shapeY.setPosition(550, shapeY_y);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        shape_y += 3;
        if (shape_y > 500)
            shape_y = 500;
        shape.setPosition(100, shape_y++);
        shapeW_y += 5;
        if (shapeW_y > 550)
            shapeW_y = 550;
        shapeW.setPosition(300, shapeW_y++);
        shapeY_y++;
        if (shapeY_y > 450)
            shapeY_y = 450;
        shapeY.setPosition(550, shapeY_y++);

        window.clear();
        
        window.draw(shape);
        window.draw(shapeW);
        window.draw(shapeY);
        
        std::this_thread::sleep_for(40ms);
        

        window.display();

    }

    return 0;
