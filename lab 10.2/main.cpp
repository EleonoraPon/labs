#include <bmp.hpp>
#include <iostream>

int main()
{
    try
    {
        el::images::BMP test_bmp(200, 100);
        test_bmp.Fill({ 0,0,200 });
        test_bmp.Rotate(acos(-1) / 4);
		test_bmp.Filling();
        test_bmp.Save("test.bmp");
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
