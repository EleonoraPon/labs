// Вариант 7. Дана матрица NхN.Найдите наибольшую сумму элементов столбца.
//Оцените сложность алгоритма в лучшем, среднем и худшем случае.Рассчитайте примерное время работы алгоритма.Запрограммируйте его.Определите реальное время работы.

#include <iostream>
#define N 10000

#include <chrono> // для функций из std::chrono

class Timer
{
private:
	// Псевдонимы типов используются для удобного доступа к вложенным типам
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};


int matrix[N][N];
int main()
{
	srand(time(0));

	int max, sum;
	max = INT_MIN;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			matrix[i][j] = rand();

	Timer t;

	for (int j = 0; j < N; j++)
	{
		sum = 0;
		for (int i = 0; i < N; i++)
			sum = sum + matrix[i][j];
		if (sum > max)
			max = sum;
	}
	
	std::cout << "Time elapsed: " << t.elapsed() << '\n';

	std::cout << max;
}
