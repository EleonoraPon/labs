#include <iostream>

double f(double a) { return a - 10; }

double Bisection(double a, double b, double e)
{
	double fa = f(a);
	while (1)
	{
		double x = (a + b) / 2;
		if (abs(a - b) < e)
			return x;
		else if (f(x) * fa > 0)
			a = x;
		else
			b = x;
	}
}

int main()
{
	double epsilon = 1e-4;
	double L = 0;
	double R = 9223372036854775807.0;  //:D
	
	double ans = Bisection(L, R, epsilon);
	std::cout << ans;

}