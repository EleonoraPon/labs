﻿#include<iostream>

/*
int main()
{
	int n = 10;
	int a, b;
	int* mas = new int[n] {23, 13, 67, 35, 46, 57, 68, 79, 31, 50};



	bool isDigitsEqualInSequence = false;
	for (int i = 0; i < n; i++)
	{
		a = mas[i];

		bool isDigitsEqual = true;
		int d = a % 10;
		a /= 10;

		while (a > 0)
		{
			b = a % 10;
			if (d != b)
			{
				isDigitsEqual = false;
				break;
			}

			a = a / 10;
		}

		if (isDigitsEqual)
			isDigitsEqualInSequence = true;
	}


	if (isDigitsEqualInSequence == false)
		for (int i = 0; i < n; i++)
			for (int k = i + 1; k < n; k++)
				if (mas[i] >= mas[k])
				{
					int ob = mas[i];
					mas[i] = mas[k];
					mas[k] = ob;
				}
	for (int i = 0; i < n; i++)
		std::cout << mas[i] << ;
	delete[] mas;
}
*/

/*
int main()
{
	int n = 10;
	int a;
	int* mas = new int[n] {23, 13, 67, 35, 46, 57, 68, 79, 31, 50};
	int* sums = new int[n]();
	int maxDigit[10];
	for (int i = 0; i < n; i++)
	{
		a = mas[i];
		sums[i] = 0;
		maxDigit[i] = 0;
		while (a > 0)
		{
			sums[i] = sums[i] + (a % 10);
			if (maxDigit[i] < (a % 10))
			{
				maxDigit[i] = (a % 10);
			}
			a = a / 10;
		}
	}

	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (sums[i] > sums[j] || (sums[i] == sums[j] && maxDigit[i] < maxDigit[j]) || (sums[i] == sums[j] && maxDigit[i] == maxDigit[j] && mas[i] < mas[j]))
			{
				int ob = sums[i];
				sums[i] = sums[j];
				sums[j] = ob;

				ob = maxDigit[i];
				maxDigit[i] = maxDigit[j];
				maxDigit[j] = ob;

				ob = mas[i];
				mas[i] = mas[j];
				mas[j] = ob;

			}
	for (int i = 0; i < n; i++)
		std::cout << mas[i] << " ";
	delete[] mas;
	delete[] sums;
}
*/

int main()
{
	int n;
	int summa, max;
	max = INT_MIN;
	std::cin >> n;
	int* sum = new int[n];

	int** matr = new int* [n];
	for (int i = 0; i < n; i++)
		matr[i] = new int[n];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			std::cin >> matr[i][j];

	for (int i = 0; i < n; i++)
	{
		summa = 0;
		for (int j = 0; j < n; j++)
		{

			summa += matr[i][j];

		}
		sum[i] = summa;
	}

	for (int i = 0; i < n; i++)
		if (sum[i] > max)
			max = sum[i];

	for (int i = 0; i < n; i++)
	{
		if (sum[i] == max)
		{
			for (int j = 0; j < n; j++)
				matr[i][j] += 1;
		}
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			std::cout << matr[i][j] << " ";

		std::cout << std::endl;
	}

	for (int i = 0; i < n; i++)
		delete[] matr[i];
	delete[] matr;


}
