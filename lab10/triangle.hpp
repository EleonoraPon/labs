#pragma once
#include <SFML/Graphics.hpp>

namespace el
{
	
	class Triangle
	{
	public:
		Triangle(float r, int x, int y, int k, float velocity);
		~Triangle();

		sf::CircleShape* Get();

		void Move();

		void SetY(int y);

		int GetY();

		int GetR();

		void SetVelocity(int velocity);

	private:
		int m_k;
		int m_x, m_y;
		float m_r;
		float m_velocity;
		sf::CircleShape* m_shape;
	};
}
