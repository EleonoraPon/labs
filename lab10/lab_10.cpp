#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <triangle.hpp>
#include <vector>


using namespace std::chrono_literals;

int main()
{
    srand(time(0));

	int width = 800;
	int height = 600;

    sf::RenderWindow window(sf::VideoMode(width, height), "SFML works!");

    const int N = 10;

    std::vector <el::Triangle*> triangles;
    for (int i = 0; i < width; i += width / N)  //создание
    {
        triangles.push_back(new el::Triangle(50, i, 150, 3, rand() % 5 + 1));
    }


    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        for (const auto& triangle : triangles) //двигаем фигурки
        {
            triangle->Move();
            if (triangle->GetY() > height - triangle->GetR())
				triangle->SetY(height - triangle->GetR());
        }

        window.clear();

        for (const auto& triangle : triangles)  //рисование
            window.draw(*triangle -> Get());

        std::this_thread::sleep_for(40ms);


        window.display();

    }

    for (const auto& triangle : triangles) //удаление
        delete triangle;
    triangles.clear();

    return 0;
}
