#include <triangle.hpp>

namespace el
{

		Triangle::Triangle(float r, int x, int y, int k, float velocity)
		{
			m_k = k;
			m_r = r;
			m_x = x;
			m_y = y;
			m_velocity = velocity;
			m_shape = new sf::CircleShape(m_r, m_k);
			m_shape->setOrigin(m_r, m_r);
			m_shape->setFillColor(sf::Color::Yellow);
			m_shape->setPosition(m_x, m_y);
		}
		Triangle::~Triangle()
		{
			delete m_shape;
		}

		sf::CircleShape* Triangle::Get() { return m_shape; }

		void Triangle::Move()
		{			
			m_y += m_velocity;
			m_shape->setPosition(m_x, m_y);
		}

		void Triangle::SetY(int y)
		{
			m_y = y;
			m_shape->setPosition(m_x, m_y);
		}

		int Triangle::GetY() { return m_y; }

		int Triangle::GetR() { return m_r; }

		void Triangle::SetVelocity(int velocity)
		{
			m_velocity = velocity;
		}
}
