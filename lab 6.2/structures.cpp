#include <iostream>

struct El_list
{
	El_list* next;
	int num;
};

void Add(El_list* head, int num)
{
	El_list* p = new El_list;
	p->num = num;

	p->next = head->next;
	head->next = p;
}

void Print(El_list* head)
{
	El_list* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->num << std::endl;
		p = p->next;
	}
}


void Del(El_list* head, int a)
{
	El_list* tmp;
	El_list* p = head;
	while (p->next != nullptr)
	{
		if (p->next->num == a)
		{
			tmp = p->next;
			p->next = p->next->next;
			delete tmp;
		}
		else
			p = p->next;
	}
}




void Duplicate(El_list* head)
{
	El_list* p = head->next;
	while (p != nullptr)
	{
		if (p->num % 2 == 1)
		{
			El_list* q = new El_list;
			q->next = p->next;
			p->next = q;
			q->num = p->num;
			p = p->next;
		}
		p = p->next;
	}
}


void Search(El_list* head, int k)
{
	El_list* p = head->next;
	while (p != nullptr)
	{
		if (p->num == k)
		{
			std::cout << p->num << std::endl;
			p = p->next;
		}
		else
			p = p->next;
	}
}


int main()
{
	El_list* head = new El_list;
	head->next = nullptr;
	
	Add(head, 2);
	Add(head, 5);
	Add(head, 7);
	Add(head, 3);
	Add(head, 9);

	Print(head);
	std::cout << "====" << std::endl;

	Del(head, 2);

	Print(head);
	std::cout << "====" << std::endl;

	Search(head, 7);

	std::cout << "====" << std::endl;

	Duplicate(head);

	Print(head);
	std::cout << "====" << std::endl;

	delete head;

	return 0;
}






/*
#include<iostream>
#include <list>
#include <iterator>

#define N 10

int main()
{
	int a = 0;

	std::list <int> listik;   //�������� ������
	//auto right = listik.begin();
	//std::list <int> ::iterator right = listik.end(); //������ �������� �� ������ ��������
	//std::list <int> ::iterator left = listik.begin(); //������ �������� �� ��������� ��������
	auto it = listik.begin();

	for (int i = 0; i < N; i++)  //���������� ��������
	{
		std::cin >> a;
		listik.push_back(a);
	}

	for (auto i = listik.begin(); i != listik.end(); i++)  //����� ���� ��������� ������ � ������� ���������
		std::cout << *i << std::endl; //*i ������� �� ������� ��������� ��������

	for (int i = 0; i < 3; i++)
		it++;

	std::cout << *it << std::endl;
	
	//std::advance(left, 3);  //������� ���������� �������� �� 3 ������� �����
	//left = listik.erase(left);
	
	//for (auto i = listik.begin(); i != listik.end(); i++)
		//std::cout << *i << std::endl;


}*/