﻿#include <iostream>
#include <fstream>

#define N 100								//исп. одну букву тк матрица квадратная!

void Read(int& n, int matrix[N][N])
{
	std::ifstream in("matr.txt");
	in >> n;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			in >> matrix[i][j];
}

bool IsDiagMax(int n, int i, int matrix[N][N])   //является ли эл диагонали матрицы максимальным?
{
	int max = matrix[i][i];
	for (int k = 0; k < n; k++)
	{
		if (matrix[i][k] > max)
		{
			return false;
		}
	}
	return true;
}

bool IsAllDiagMax(int n, int matrix[N][N])        //все эл диагнали макс??
{
	for (int i = 0; i < n; i++)
	{
		if (!IsDiagMax(n, i, matrix))
		{
			return false;
		}
	}
	return true;
}

int Mult(int n, int matrix[N][N])          //произведение
{
	int productn = 1;
	for (int i = 0; i < n; i++)
	{
		productn = productn * matrix[i][i];
	}
	return productn;
}


bool ZeroCheck(int x)			//содержит ли 0?
{
	if (x == 0)
		return true;
	while (x > 0)
	{
		if(x % 10 == 0)              //рфспаковка
			return true;
		x = x / 10;
	}
	return false;
}

void Replacement(int n, int matrix[N][N])    //замена
{
	int multiplication = Mult(n, matrix);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
		{
			if (ZeroCheck(matrix[i][j]))
				matrix[i][j] = multiplication;
		}
}

void Write(int n, int matrix[N][N])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			std::cout << matrix[i][j] << " ";
		std::cout << std::endl;
	}
}

int main()
{
	int matrix[N][N];
	int n;							//оставить м тк без неё матрица выводится как попало 
	
	int  productn = 1;
	bool maxi = true;

	Read(n, matrix);
	
	if (IsAllDiagMax)
	{
		Replacement(n, matrix);
	}

	Write(n, matrix);

	return 0;
}