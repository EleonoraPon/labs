﻿#include <iostream>

int main()
{
    double a, b, c, d;
    
    std::cin >> a >> b >> c;
    if (a < 30000 && b < 30000 && c < 30000)
    {
        d = sqrt(a*a + b*b + c*c);
        std::cout << d << std::endl;
    }
    else
    {
        return 0;
    }
}

