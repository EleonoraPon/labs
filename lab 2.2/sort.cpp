#include <iostream>
#include <chrono>

#define N 10

void QuickSort(int a, int b, int* mas)     
{
	if (a >= b)
		return;
	int m = ((rand()*rand()) % (b-a+1)) + a;                    
	int k = mas[m];            
	int l = a - 1;                           //����� �������
	int r = b + 1;                          //������ ������� 
	while (1)                             
	{
		do
		{
			l += 1;
		} while (mas[l] < k);
		do 
		{
			r -= 1;
		} while (mas[r] > k);
			
		if (l >= r)
			break;
		std::swap(mas[r], mas[l]); 
	}
	r = l;
	l = l - 1;
	QuickSort(a, l, mas);                                   
	QuickSort(r, b, mas);                                   
}


void BubleSort(int* mas, int n) //��������
{
	for (int i = 1; i < n; i++)
	{
		if (mas[i] >= mas[i - 1])
		{
			continue;
		}
		int j = i - 1;
		while (j >= 0 && mas[j] > mas[j + 1])
		{
			std::swap(mas[j], mas[j + 1]);
			j = j - 1;
		}
	}

}

int main()
{
	int* mass = new int [N] {};
	int* masi = new int [N] {};
	srand(time(0));

	for (int i = 0; i < N; i++)
	{
		mass[i] = rand();
	}
	for (int i = 0; i < N; i++)
	{
		masi[i] = mass[i];
	}

	QuickSort(0, N-1, mass);
	std::cout << '\n'; 

	for (int i = 0; i < N; i++)
	{
		std::cout << mass[i] << " ";
	}

	
	BubleSort(masi, N);
	std::cout << '\n';

	for (int i = 0; i < N; i++)
	{
		std::cout << masi[i] << " ";
	}

	delete[] mass;
	delete[] masi;
}