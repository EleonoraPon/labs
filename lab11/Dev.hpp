#pragma once
#include <iostream>

namespace el
{
	class Device
	{
	public:
		Device(std::string name, int adress);
		~Device();

		void Poll();
		void Print();

	private:
		std::string m_name;
		int m_adress;
		bool in1, in2;
	};
	
	
}
