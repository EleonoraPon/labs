﻿#include "Dev.hpp"
#include <iostream>
#include <vector>

int main()
{
    std::vector <el::Device*> dev;

    dev.push_back(new el::Device("A", 1));
    dev.push_back(new el::Device("B", 1));

    for (auto& per : dev)
    {
        per->Poll();
        per->Print();
    }
    for (int i = 0; i < dev.size(); i++)
    {
        delete dev[i];
    }
    return 0;
}
